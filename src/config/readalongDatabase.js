const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ReadalongSchema = new Schema({
  naam: {
    type: String,
    required: [true, "naam is required"],
  },
  startDatum: {
    type: Date,
    required: [true, "startDatum is required"],
  },
  eindDatum: {
    type: Date,
    required: [true, "eindDatum is required"],
  },
  readalongType: {
    type: String,
    required: [true, "readalongType is required"],
  },
  minimumDeelnemers: {
    type: Number,
    required: [true, "minimunDeelnemers is required"],
  },
  boekId: {
    type: String,
    required: [true, "BoekId is required"],
  },
  userId: [String],
});

ReadalongSchema.virtual("voldoendeDeelnemers").get(function () {
  return false;
});

const Readalong = mongoose.model("readalong", ReadalongSchema);

module.exports = Readalong;
