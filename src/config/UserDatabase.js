const instance = require("./databaseConnect");

instance.model("User", {
  user_id: {
      primary: true,
      type: "uuid",
      required: true,
    },
    naam: {type: "string", required: true},
    gebruikersnaam: {type: "string", required: true},
    wachtwoord: {type: "stirng", required: true},
    email: {type: "string", required: true},
    role: {type: "string", required: true},
  });

  module.exports = instance;
