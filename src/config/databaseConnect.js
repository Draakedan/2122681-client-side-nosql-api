var Neode = require("neode");

const instance = new Neode(
  process.env.NEO_URL || "bolt://localhost:7687",
  process.env.NEO_USER || "BookclubUser",
  process.env.NEO_PASS || "W9o@BpZ2N6&",
  true
);

instance.model("User", {
  user_id: {
    primary: true,
    type: "uuid",
    required: true,
  },
  naam: { type: "string", required: true },
  gebruikersnaam: { type: "string", required: true },
  wachtwoord: { type: "stirng", required: true },
  email: { type: "string", required: true },
  role: { type: "string", required: true },
});

instance.model("User").relationship("following", "FOLLOWING", "out", "User");

instance.model("Book", {
  book_neo_id: {
    primary: true,
    type: "uuid",
    required: true,
  },
  book_mongo_id: { type: "string", required: true },
});

instance.model("User").relationship("creator", "MADE_BY", "out", "Book");
instance.model("Book").relationship("created", "MADE_BY", "in", "User");

instance.model("Readalong", {
  readalong_neo_id: {
    primary: true,
    type: "uuid",
    required: true,
  },
  readalong_mongo_id: { type: "string", required: true },
});

instance.model("User").relationship("creator", "MADE_BY", "out", "Readalong");
instance.model("Readalong").relationship("created", "MADE_BY", "in", "User");

instance.model("User").relationship("enter", "ENTERED_IN", "out", "Readalong");
instance.model("Readalong").relationship("joined", "ENTERED_IN", "in", "User");

instance.model("Bookclub", {
  bookclub_neo_id: {
    primary: true,
    type: "uuid",
    required: true,
  },
  bookclub_mongo_id: { type: "string", required: true },
});

instance.model("User").relationship("creator", "MADE_BY", "out", "Bookclub");
instance.model("Bookclub").relationship("created", "MADE_BY", "in", "User");

instance.model("User").relationship("member", "MEMBER_OF", "out", "Bookclub");
instance.model("Bookclub").relationship("joined", "MEMBER_OF", "in", "User");

module.exports = instance;
