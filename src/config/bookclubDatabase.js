const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const BookclubSchema = new Schema({
  naam: {
    type: String,
    required: [true, "naam is required"],
  },
  favBoekId: {
    type: String,
    required: [true, "favBoekId is reqruied"],
  },
  hoofdGenre: {
    type: String,
    required: [true, "hoofdGenre is required"],
  },
  readalongs: [String],
  aanTeRadenBoeken: [String],
  userId: [String],
});

const bookclub = mongoose.model("bookclub", BookclubSchema);

module.exports = bookclub;
