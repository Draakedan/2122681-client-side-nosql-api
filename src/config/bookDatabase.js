const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const BookSchema = new Schema({
  titel: {
    type: String,
    required: [true, "Titel is required"],
  },
  autheur: {
    type: String,
    required: [true, "Auther is required"],
  },
  isbn: {
    type: Number,
    required: [true, "Isbn is required"],
  },
  eersteUitgave: {
    type: Date,
    required: [true, "Eerste uitgave is required"],
  },
  paginas: {
    type: Number,
    required: [true, "Paginas is required"],
  },
  genre: {
    type: String,
    required: [true, "Genre is required"],
  },
  userId: [String],
});

const Book = mongoose.model("book", BookSchema);

module.exports = Book;
