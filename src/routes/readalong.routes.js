const express = require("express");
const router = express.Router();
const readalongs = require("../controllers/readalong.controller");
const authController = require("../controllers/authentication.controller");

router.post(
  "/readalongs",
  authController.validateToken,
  readalongs.addOneReadalong
);
router.get(
  "/readalongs",
  authController.validateToken,
  readalongs.getAllReadalongs
);
router.get(
  "/readalongs/:id",
  authController.validateToken,
  readalongs.getOneReadalong
);
router.put(
  "/readalongs/:id",
  authController.validateToken,
  readalongs.UpdateOneReadalong
);
router.post(
  "/readalongs/:id",
  authController.validateToken,
  readalongs.DeleteOneReadalong
);
router.get(
  "/readalongs/:id/members",
  authController.validateToken,
  readalongs.getAllMembers
);
router.post(
  "/readalongs/:id/members/register",
  authController.validateToken,
  readalongs.registerMember
);
router.put(
  "/readalongs/:id/members/unregister",
  authController.validateToken,
  readalongs.unregisterMember
);

module.exports = router;
