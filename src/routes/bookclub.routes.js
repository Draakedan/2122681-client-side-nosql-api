const express = require("express");
const router = express.Router();
const bookclubs = require("../controllers/bookclub.controller");
const authController = require("../controllers/authentication.controller");

router.get("/bookclubs", bookclubs.getAllBookClubs);
router.post(
  "/bookclubs",
  authController.validateToken,
  bookclubs.addOneBookclub
);
router.get(
  "/bookclubs/:id",
  authController.validateToken,
  bookclubs.getOneBookclub
);
router.put(
  "/bookclubs/:id",
  authController.validateToken,
  bookclubs.updateOneBookclub
);
router.post(
  "/bookclubs/:id",
  authController.validateToken,
  bookclubs.deleteOneBookclub
);
router.get(
  "/bookclubs/:id/readalongs",
  authController.validateToken,
  bookclubs.getAllReadalongs
);
router.post(
  "/bookclubs/:id/readalongs/register",
  authController.validateToken,
  bookclubs.registerReadalong
);
router.put(
  "/bookclubs/:id/readalongs/unregister",
  authController.validateToken,
  bookclubs.unregisterReadalong
);
router.get(
  "/bookclubs/:id/books",
  authController.validateToken,
  bookclubs.getAllBooks
);
router.post(
  "/bookclubs/:id/books/register",
  authController.validateToken,
  bookclubs.registerBook
);
router.put(
  "/bookclubs/:id/books/unregister",
  authController.validateToken,
  bookclubs.unregisterBook
);
router.get(
  "/bookclubs/:id/members",
  authController.validateToken,
  bookclubs.getAllMembers
);
router.post(
  "/bookclubs/:id/members/register",
  authController.validateToken,
  bookclubs.registerMember
);
router.put(
  "/bookclubs/:id/members/unregister",
  authController.validateToken,
  bookclubs.unregisterMember
);

module.exports = router;
