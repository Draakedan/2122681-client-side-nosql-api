const express = require("express");
const router = express.Router();
const books = require("../controllers/book.controller");
const authController = require("../controllers/authentication.controller");

router.post("/books", authController.validateToken, books.addOneBook);
router.get("/books", books.getAllBooks);
router.get("/books/:id", authController.validateToken, books.getOneBook);
router.post("/books/:id", authController.validateToken, books.deleteOneBook);
router.put("/books/:id", authController.validateToken, books.updateOneBook);

module.exports = router;
