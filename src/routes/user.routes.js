const express = require("express");
const router = express.Router();
const users = require("../controllers/user.controller");
const authController = require("../controllers/authentication.controller");

router.get("/users", authController.validateToken, users.getAllUsers);
router.post("/users", users.addOneUser);
router.get("/users/:id", authController.validateToken, users.getOneUser);
router.put("/users/:id", authController.validateToken, users.updateOneUser);
router.delete("/users/:id", authController.validateToken, users.deleteOneUser);
router.get(
  "/users/:id/following",
  authController.validateToken,
  users.showAllfollowers
);
router.post(
  "/users/:id/follow",
  authController.validateToken,
  users.followOneUser
);
router.put(
  "/users/:id/unfollow",
  authController.validateToken,
  users.unfollowOneUser
);
router.get(
  "/users/:id/following/books",
  authController.validateToken,
  users.getAllBooks
);

module.exports = router;
