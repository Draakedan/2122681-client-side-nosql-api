var instance = require("../config/databaseConnect");
const config = require("../config/config");
const logger = config.logger;

let controller = {
  getAllUsers(req, res, next) {
    if (req.method != "GET") next();
    logger.info("GET called from /api/user/users/");

    try {
      instance.all("User").then((results) => {
        let returnable = [];
        for (let i = 0; i < results.length; i++) {
          returnable.push({
            user_id: results.get(i).get("user_id"),
            naam: results.get(i).get("naam"),
            gebruikersnaam: results.get(i).get("gebruikersnaam"),
            wachtwoord: results.get(i).get("wachtwoord"),
            email: results.get(i).get("email"),
            role: results.get(i).get("role"),
          });
        }
        logger.info(returnable);
        res.send(returnable);
      });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  addOneUser(req, res, next) {
    if (req.method != "POST") next();
    logger.info("POST called from /api/user/users/");
    logger.info(req.body);

    try {
      instance
        .create("User", {
          naam: req.body.naam,
          gebruikersnaam: req.body.gebruikersnaam,
          wachtwoord: req.body.wachtwoord,
          email: req.body.email,
          role: req.body.role,
        })
        .then((results) => {
          instance
            .first("User", { user_id: results.get("user_id") })
            .then((user) => {
              res.send({
                user_id: user.get("user_id"),
                naam: user.get("naam"),
                gebruikersnaam: user.get("gebruikersnaam"),
                wachtwoord: user.get("wachtwoord"),
                email: user.get("email"),
                role: user.get("role"),
              });
            });
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  getOneUser(req, res, next) {
    if (req.method != "GET") next();
    logger.info("GET called from /api/user/users/:id");

    try {
      instance.first("User", { user_id: req.params.id }).then((user) => {
        console.log(user.get("naam"));
        res.send({
          user_id: user.get("user_id"),
          naam: user.get("naam"),
          gebruikersnaam: user.get("gebruikersnaam"),
          wachtwoord: user.get("wachtwoord"),
          email: user.get("email"),
          role: user.get("role"),
        });
      });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  updateOneUser(req, res, next) {
    if (req.method != "PUT") next();
    logger.info("PUT called from /api/user/users/:id");

    try {
      instance.first("User", { user_id: req.params.id }).then((user) => {
        user
          .update({
            naam: req.body.naam,
            gebruikersnaam: req.body.gebruikersnaam,
            wachtwoord: req.body.wachtwoord,
            email: req.body.email,
            role: req.body.role,
          })
          .then(
            instance.first("User", { user_id: req.params.id }).then((user) => {
              console.log(user.get("naam"));
              res.send({
                user_id: user.get("user_id"),
                naam: user.get("naam"),
                gebruikersnaam: user.get("gebruikersnaam"),
                wachtwoord: user.get("wachtwoord"),
                email: user.get("email"),
                role: user.get("role"),
              });
            })
          );
      });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  deleteOneUser(req, res, next) {
    if (req.method != "DELETE") next();
    logger.info("DELETE called from /api/user/users/:id");

    try {
      instance.first("User", { user_id: req.params.id }).then((user) => {
        user.delete();
        res.send({ status: "deleted" });
      });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  showAllfollowers(req, res, next) {
    if (req.method != "GET") next();
    logger.info("GET called from /api/user/users/:id/following");
    try {
      instance
        .cypher(
          "MATCH (User)-[:out]->(users:User) WHERE User.user_id= $id RETURN users",
          { id: req.params.id }
        )
        .then((results) => {
          return Promise.all([
            instance.hydrate(results, "users").toJson(),
          ]).then((users) => {
            console.log(users);
            return users;
          });
        })
        .then((users) => {
          logger.info(users);
          res.send(users);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  followOneUser(req, res, next) {
    if (req.method != "POST") next();
    logger.info("POST called from /api/user/users/:id/follow");
    try {
      instance.first("User", { user_id: req.body.id }).then((user) => {
        instance.first("User", { user_id: req.params.id }).then((user2) => {
          user.relateTo(user2, "following");
          res.send(user.get("naam" + "is following" + user2.get("naam")));
        });
      });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  unfollowOneUser(req, res, next) {
    if (req.method != "PUT") next();
    logger.info("PUT called from /api/user/users/:id/unfollow");
    try {
      instance.first("User", { user_id: req.body.id }).then((user) => {
        instance.first("User", { user_id: req.params.id }).then((user2) => {
          user.detachFrom(user2);
          res.send(user.get("naam" + "has unfollowed" + user2.get("naam")));
        });
      });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  getAllBooks(req, res, next) {
    if (req.method != "GET") next();
    logger.info("GET called from /api/user/users/:id/following/books");

    try {
      instance
        .cypher(
          "Match(User {user_id: $id})-[:out]->(user:User)-[:out]->(books:Book) return books",
          { id: req.params.id }
        )
        .then((results) => {
          return Promise.all([
            instance.hydrate(results, "books").toJson(),
          ]).then((users) => {
            console.log(users);
            return users;
          });
        })
        .then((users) => {
          logger.info(users);
          res.send(users);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },
};

module.exports = controller;
