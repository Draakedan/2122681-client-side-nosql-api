const config = require("../config/config");
var instance = require("../config/databaseConnect");
const jwt = require("jsonwebtoken");
const logger = config.logger;

let controller = {
  login(req, res, next) {
    try {
      if (req.body.gn == undefined || req.body.ww == undefined) {
        res.status(500).send("Login failed");
        return;
      }
      instance
        .first("User", { gebruikersnaam: req.body.gn, wachtwoord: req.body.ww })
        .then((user) => {
          const payload = {
            gebruikersnaam: req.body.gn,
            wachtwoord: req.body.ww,
          };
          //logger.info("user" + user);

          const userinfo = {
            token: jwt.sign(payload, process.env.JWT_SECRET, {
              expiresIn: "1h",
            }),
            user_id: user.get("user_id"),
            naam: user.get("naam"),
                gebruikersnaam: user.get("gebruikersnaam"),
                email: user.get("email"),
                role: user.get("role")
          };
          res.send(userinfo);
        });
    } catch (error) {
      res.status(500).send("Login failed");
    }
  },

  validateToken(req, res, next) {
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      logger.warn("Authorizaton header is missing!");
      res
        .status(401)
        .send("Authorizaton header is missing! " + new Date().toISOString());
      return;
    }

    const token = authHeader.substring(7, authHeader.length);

    jwt.verify(token, process.env.JWT_SECRET, (error, payload) => {
      if (error) {
        res.status(401).send(error.toString() + new Date().toISOString());
        return;
      }

      if (payload) {

        req.userId = payload.id;
        next();
      }
    });
  },
};

module.exports = controller;
