var instance = require("../config/databaseConnect");
const Book = require("../config/bookDatabase");
const config = require("../config/config");
const bookclubs = require("./bookclub.controller");
const readalongs = require("./readalong.controller");
const logger = config.logger;

let controller = {
  getAllBooks(req, res, next) {
    if (req.method != "GET") next();
    logger.info("GET called from /api/book/books");

    try {
      Book.find({})
        .lean()
        .exec(function (err, docs) {
          res.send(docs);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  addOneBook(req, res, next) {
    if (req.method != "POST") next();
    logger.info("POST called from /api/book/books");

    if (
      req.body.titel == undefined ||
      req.body.autheur == undefined ||
      req.body.isbn == undefined ||
      req.body.eersteUitgave == undefined ||
      req.body.paginas == undefined ||
      req.body.genre == undefined ||
      req.body.id == undefined
    ) {
      res.status(500).send("create book failed");
      return;
    }

    const newBook = new Book({
      titel: req.body.titel,
      autheur: req.body.autheur,
      isbn: req.body.isbn,
      eersteUitgave: req.body.eersteUitgave,
      paginas: req.body.paginas,
      genre: req.body.genre,
      userId: req.body.id,
    });

    try {
      newBook.save().then(() => {
        instance
          .create("Book", { book_mongo_id: newBook._id.toString() })
          .then((neoBook) => {
            instance.first("User", { user_id: req.body.id }).then((user) => {
              neoBook.relateTo(user, "created");
              user.relateTo(neoBook, "creator");
              logger.info(neoBook + " is made by " + user);
            });
          });

        return res.send(newBook);
      });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  getOneBook(req, res, next) {
    if (req.method != "GET") next();
    logger.info("GET called from /api/book/books/:id");

    try {
      Book.findById(req.params.id)
        .lean()
        .exec(function (err, docs) {
          res.send(docs);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  deleteOneBook(req, res, next) {
    if (req.method != "POST") next();
    logger.info("DELETE called from /api/book/books/:id");

    try {
      logger.info(req.params.id);
      logger.info(req.body.id);
      Book.findByIdAndDelete(req.params.id)
        .lean()
        .exec(function (err, docs) {
          bookclubs.removeBookFromList(req.params.id);
          bookclubs.deleteBookclubByBook(req.params.id);
          readalongs.deleteReadalongByBook(req.params.id);
          instance
            .first("Book", { book_mongo_id: req.params.id })
            .then((neoBook) => {
              instance.first("User", { user_id: req.body.id }).then((user) => {
                neoBook.detachFrom(user);
                user.detachFrom(neoBook);
                neoBook.delete();
              });
            });

          res.send(docs);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  updateOneBook(req, res, next) {
    if (req.method != "PUT") next();
    logger.info("PUT called from /api/book/books/:id");

    try {
      Book.findByIdAndUpdate(req.params.id, req.body)
        .lean()
        .exec(function (err, docs) {
          res.send(docs);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },
};

module.exports = controller;
