var instance = require("../config/databaseConnect");
const config = require("../config/config");
const bookclubs = require("./bookclub.controller");
const logger = config.logger;
const Readalong = require("../config/readalongDatabase");

let controller = {
  getAllReadalongs(req, res, next) {
    if (req.method != "GET") next();
    logger.info("GET called from /api/readalong/readalongs");

    try {
      Readalong.find({})
        .lean()
        .exec(function (err, docs) {
          logger.info(docs);
          res.send(docs);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  addOneReadalong(req, res, next) {
    if (req.method != "POST") next();
    logger.info("POST called from /api/readalong/readalongs");

    if (
      req.body.naam == undefined ||
      req.body.startDatum == undefined ||
      req.body.eindDatum == undefined ||
      req.body.readalongType == undefined ||
      req.body.minimumDeelnemers == undefined ||
      req.body.boekId == undefined ||
      req.body.id == undefined
    ) {
      res.status(500).send("create readalong failed");
      return;
    }

    try {
      const newReadalong = new Readalong({
        naam: req.body.naam,
        startDatum: req.body.startDatum,
        eindDatum: req.body.eindDatum,
        readalongType: req.body.readalongType,
        minimumDeelnemers: req.body.minimumDeelnemers,
        boekId: req.body.boekId,
        userId: req.body.id,
      });

      newReadalong.save().then(() => {
        instance
          .create("Readalong", {
            readalong_mongo_id: newReadalong._id.toString(),
          })
          .then((neoReadalong) => {
            instance.first("User", { user_id: req.body.id }).then((user) => {
              neoReadalong.relateTo(user, "created");
              user.relateTo(neoReadalong, "creator");
              logger.info(neoReadalong + " is made by " + user);
            });
          });

        return res.send(newReadalong);
      });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  getOneReadalong(req, res, next) {
    if (req.method != "GET") next();
    logger.info("GET called from /api/readalong/readalongs/:id");

    try {
      Readalong.findById(req.params.id)
        .lean()
        .exec(function (err, docs) {
          logger.info(docs);
          res.send(docs);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  UpdateOneReadalong(req, res, next) {
    if (req.method != "PUT") next();
    logger.info("PUT called from /api/readalong/readalongs/:id");

    try {
      Readalong.findByIdAndUpdate(req.params.id, req.body)
        .lean()
        .exec(function (err, docs) {
          logger.info(docs);
          res.send(docs);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  DeleteOneReadalong(req, res, next) {
    if (req.method != "POST") next();
    logger.info("DELETE called from /api/readalong/readalongs/:id");

    if (req.body.id == undefined) {
      res.status(500).send("del readalong failed");
      return;
    }

    try {
      Readalong.findByIdAndDelete(req.params.id)
        .lean()
        .exec(function (err, docs) {
          bookclubs.removeReadalongFromList(req.params.id)
          logger.info("readalong deleted");
          instance
            .first("Readalong", { readalong_mongo_id: req.params.id })
            .then((neoReadalong) => {
              instance.first("User", { user_id: req.body.id }).then((user) => {
                neoReadalong.detachFrom(user);
                user.detachFrom(neoReadalong);
                neoReadalong.delete();
              });
            });
          res.send(docs);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  deleteReadalongByBook(id) {
    try {
      Readalong.find({ boekId: id })
        .lean()
        .exec(function (err, docs) {
          console.log(docs)
          var queries = [];
          docs.forEach(function (d) {
            queries.push(
              Readalong.findByIdAndDelete(d._id)
                .then(function (errs, docss) {
                  bookclubs.removeReadalongFromList(d._id)
                  instance
                    .first("Readalong", { readalong_mongo_id: d._id.toString() })
                    .then((neoReadalong) => {
                      instance
                        .first("User", { user_id: d.userId[0] })
                        .then((user) => {
                          logger.info(neoReadalong)
                          logger.info(user)
                          neoReadalong.detachFrom(user);
                          user.detachFrom(neoReadalong);
                          neoReadalong.delete();
                        });
                    });
                })
            );
          });

          return Promise.all(queries);
        });
    } catch (error) {
      loger.error(error);
    }
  },

  getAllMembers(req, res, next) {
    if (req.method != "GET") next();
    logger.info("GET called from /api/readalong/readalongs/:id/members");

    try {
      instance
        .cypher(
          "MATCH (Readalong)-[:in]->(users:User) WHERE Readalong.readalong_mongo_id= $id RETURN users",
          { id: req.params.id }
        )
        .then((results) => {
          return Promise.all([
            instance.hydrate(results, "users").toJson(),
          ]).then((users) => {
            console.log(users);
            return users;
          });
        })
        .then((users) => {
          logger.info(users);
          res.send(users);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  registerMember(req, res, next) {
    if (req.method != "POST") next();
    logger.info(
      "POST called from /api/readalong/readalongs/:id/members/register"
    );
    if (req.body.id == undefined) {
      res.status(500).send("create readalong failed");
      return;
    }

    logger.info(req.body);

    try {
      instance
        .first("Readalong", { readalong_mongo_id: req.params.id })
        .then((neoReadalong) => {
          instance.first("User", { user_id: req.body.id }).then((user) => {
            user.relateTo(neoReadalong, "enter");
            neoReadalong.relateTo(user, "joined");
            res.send({ id: "Success!" });
          });
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  unregisterMember(req, res, next) {
    if (req.method != "PUT") next();
    logger.info(
      "PUT called from /api/readalong/readalongs/:id/members/unregister"
    );

    if (req.body.id == undefined) {
      res.status(500).send("create readalong failed");
      return;
    }

    try {
      instance
        .first("Readalong", { readalong_mongo_id: req.params.id })
        .then((neoReadalong) => {
          instance.first("User", { user_id: req.body.id }).then((user) => {
            user.detachFrom(neoReadalong);
            neoReadalong.detachFrom(user);
            res.send({ id: "Success!" });
          });
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },
};

module.exports = controller;
