var instance = require("../config/databaseConnect");
const config = require("../config/config");
const logger = config.logger;
const bookclub = require("../config/bookclubDatabase");

let controller = {
  getAllBookClubs(req, res, next) {
    if (req.method != "GET") next();
    logger.info("GET called from /api/bookclub/bookclubs");

    try {
      bookclub
        .find({})
        .lean()
        .exec(function (err, docs) {
          logger.info(docs);
          res.send(docs);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  addOneBookclub(req, res, next) {
    if (req.method != "POST") next();
    logger.info("POST called from /api/bookclub/bookclubs");

    try {
      const newBookclub = new bookclub({
        naam: req.body.naam,
        favBoekId: req.body.favBoekId,
        hoofdGenre: req.body.hoofdGenre,
        userId: req.body.id,
      });

      newBookclub.save().then(() => {
        logger.info(newBookclub._id.toString());
        instance
          .create("Bookclub", { bookclub_mongo_id: newBookclub._id.toString() })
          .then((neoBookclub) => {
            instance.first("User", { user_id: req.body.id }).then((user) => {
              neoBookclub.relateTo(user, "created");
              user.relateTo(neoBookclub, "creator");
              logger.info(neoBookclub + " is made by " + user);
            });
          });

        return res.send(newBookclub);
      });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  getOneBookclub(req, res, next) {
    if (req.method != "GET") next();
    logger.info("GET called from /api/bookclub/bookclubs/:id");

    try {
      bookclub
        .findById(req.params.id)
        .lean()
        .exec(function (err, docs) {
          logger.info(docs);
          res.send(docs);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  updateOneBookclub(req, res, next) {
    if (req.method != "PUT") next();
    logger.info("PUT called from /api/bookclub/bookclubs/:id");

    try {
      bookclub
        .findByIdAndUpdate(req.params.id, req.body)
        .lean()
        .exec(function (err, docs) {
          logger.info(docs);
          res.send(docs);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  deleteOneBookclub(req, res, next) {
    if (req.method != "POST") next();
    logger.info("DELETE called from /api/bookclub/bookclubs/:id");

    try {
      bookclub
        .findByIdAndDelete(req.params.id)
        .lean()
        .exec(function (err, docs) {
          logger.info(docs);

          instance
            .first("Bookclub", { bookclub_mongo_id: req.params.id })
            .then((neoBookclub) => {
              instance.first("User", { user_id: req.body.id }).then((user) => {
                neoBookclub.detachFrom(user);
                user.detachFrom(neoBookclub);
                neoBookclub.delete();
              });
            });
          res.send(docs);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  getAllReadalongs(req, res, next) {
    if (req.method != "GET") next();
    logger.info("GET called from /api/bookclub/bookclubs/:id/readalongs");

    try {
      bookclub
        .findById(req.params.id)
        .lean()
        .exec(function (err, docs) {
          logger.info(docs.readalongs);
          res.send(docs.readalongs);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  registerReadalong(req, res, next) {
    if (req.method != "POST") next();
    logger.info(
      "POST called from /api/bookclub/bookclubs/:id/readalongs/register"
    );

    try {
      bookclub
        .findByIdAndUpdate(req.params.id, {
          $push: { readalongs: req.body.readalongId },
        })
        .lean()
        .exec(function (err, docs) {
          logger.info(docs.readalongs);
          res.send(docs.readalongs);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  unregisterReadalong(req, res, next) {
    if (req.method != "PUT") next();
    logger.info(
      "PUT called from /api/bookclub/bookclubs/:id/readalongs/unregister"
    );

    try {
      bookclub
        .findByIdAndUpdate(req.params.id, {
          $pull: { readalongs: req.body.readalongId },
        })
        .lean()
        .exec(function (err, docs) {
          logger.info(docs.readalongs);
          res.send(docs.readalongs);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  getAllBooks(req, res, next) {
    if (req.method != "GET") next();
    logger.info("GET called from /api/bookclub/bookclubs/:id/books");

    try {
      bookclub
        .findById(req.params.id)
        .lean()
        .exec(function (err, docs) {
          logger.info(docs.aanTeRadenBoeken);
          res.send(docs.aanTeRadenBoeken);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  registerBook(req, res, next) {
    if (req.method != "POST") next();
    logger.info("POST called from /api/bookclub/bookclubs/:id/books/register");

    try {
      bookclub
        .findByIdAndUpdate(req.params.id, {
          $push: { aanTeRadenBoeken: req.body.bookId },
        })
        .lean()
        .exec(function (err, docs) {
          logger.info(docs.aanTeRadenBoeken);
          res.send(docs.aanTeRadenBoeken);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  deleteBookclubByBook(id) {
    try {
      bookclub
        .find({ favBoekId: id })
        .lean()
        .exec(function (err, docs) {
          var queries = [];
          docs.forEach(function (d) {
            queries.push(
              bookclub
                .findByIdAndUpdate(docs._id, {
                  $pull: { aanTeRadenBoeken: id },
                })
                .then(function (e, doc) {
                  bookclub
                    .findByIdAndDelete(d._id)
                    .then(function (errs, docss) {
                      instance
                        .first("Bookclub", {
                          bookclub_mongo_id: d._id.toString(),
                        })
                        .then((neoBookclub) => {
                          instance
                            .first("User", { user_id: d.userId[0] })
                            .then((user) => {
                              neoBookclub.detachFrom(user);
                              user.detachFrom(neoBookclub);
                              neoBookclub.delete();
                            });
                        });
                    });
                })
            );
          });

          return Promise.all(queries);
        });
    } catch (error) {
      loger.error(error);
    }
  },

  removeBookFromList(id) {
    try {
      bookclub
        .find({})
        .lean()
        .exec(function (err, docs) {
          var queries = [];
          docs.forEach(function (d) {
            queries.push(
              bookclub
                .findByIdAndUpdate(d._id, {
                  $pull: { aanTeRadenBoeken: id },
                })
                .lean()
                .exec(function (err, doc) {
                })
            );
          });

          return Promise.all(queries);
        });
    } catch (error) {
      loger.error(error);
    }
  },

  removeReadalongFromList(id) {
    try {
      bookclub
        .find({})
        .lean()
        .exec(function (err, docs) {
          var queries = [];
          docs.forEach(function (d) {
            queries.push(
              bookclub
                .findByIdAndUpdate(d._id, {
                  $pull: { readalongs: id },
                })
                .lean()
                .exec(function (err, doc) {
                })
            );
          });

          return Promise.all(queries);
        });
    } catch (error) {
      loger.error(error);
    }
  },

  unregisterBook(req, res, next) {
    if (req.method != "PUT") next();
    logger.info("PUT called from /api/bookclub/bookclubs/:id/books/unregister");

    try {
      bookclub
        .findByIdAndUpdate(req.params.id, {
          $pull: { aanTeRadenBoeken: req.body.bookId },
        })
        .lean()
        .exec(function (err, docs) {
          logger.info(docs.aanTeRadenBoeken);
          res.send(docs.aanTeRadenBoeken);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  getAllMembers(req, res, next) {
    if (req.method != "GET") next();
    logger.info("GET called from /api/bookclub/bookclubs/:id/members");
    try {
      instance
        .cypher(
          "MATCH (Bookclub)-[:in]->(users:User) WHERE Bookclub.bookclub_mongo_id= $id RETURN users",
          { id: req.params.id }
        )
        .then((results) => {
          return Promise.all([
            instance.hydrate(results, "users").toJson(),
          ]).then((users) => {
            console.log(users);
            return users;
          });
        })
        .then((users) => {
          logger.info(users);
          res.send(users);
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  registerMember(req, res, next) {
    if (req.method != "POST") next();
    logger.info(
      "POST called from /api/bookclub/bookclubs/:id/members/register"
    );

    try {
      instance
        .first("Bookclub", { bookclub_mongo_id: req.params.id })
        .then((neoBookclub) => {
          instance.first("User", { user_id: req.body.id }).then((user) => {
            user.relateTo(neoBookclub, "member");
            neoBookclub.relateTo(user, "joined");
            res.send({ id: "Success!" });
          });
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },

  unregisterMember(req, res, next) {
    if (req.method != "PUT") next();
    logger.info(
      "PUT called from /api/bookclub/bookclubs/:id/members/unregister"
    );
    try {
      instance
        .first("Bookclub", { bookclub_mongo_id: req.params.id })
        .then((neoBookclub) => {
          instance.first("User", { user_id: req.body.id }).then((user) => {
            user.detachFrom(neoBookclub);
            neoBookclub.detachFrom(user);
            res.send({ id: "Success!" });
          });
        });
    } catch (error) {
      res.status(500).send(error);
    }
  },
};

module.exports = controller;
