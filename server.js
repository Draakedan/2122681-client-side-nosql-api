const mongoose = require("mongoose");
const express = require("express");
const bodyParser = require("body-parser");
const config = require("./src/config/config");
const bookRoutes = require("./src/routes/book.routes");
const readalongRoutes = require("./src/routes/readalong.routes");
const bookclubRoutes = require("./src/routes/bookclub.routes");
const userRoutes = require("./src/routes/user.routes");
const authRouts = require("./src/routes/authentication.routes");

const logger = config.logger;
const app = express();

mongoose.connect(
  process.env.MONGO_URL || "mongodb://localhost:27017/bookclubs"
);

app.use(bodyParser.json());

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type,authorization"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

port = process.env.PORT || 3000;

app.all("*", (req, res, next) => {
  const method = req.method;
  logger.info("Method: ", method);
  next();
});

app.use("/api", authRouts);
app.use("/api", bookRoutes);
app.use("/api", readalongRoutes);
app.use("/api", bookclubRoutes);
app.use("/api", userRoutes);

app.all("*", (req, res, next) => {
  logger.error("error: route not available");
  err.sendError404(res, "route not available");
});

app.listen(port, () => {
  logger.info(`Server running at http://:${port}/`);
});

module.exports = app;
