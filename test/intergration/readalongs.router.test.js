const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../server");
const tracer = require("tracer");
const config = require("../../src/config/config");
const jwt = require("jsonwebtoken");
const instance = require("../../src/config/databaseConnect");
const Readalong = require("../../src/config/readalongDatabase");

const logger = config.logger;

chai.should();
chai.use(chaiHttp);
tracer.setLevel("error");

let userId;
let userName = "test123";
let userPass = "t@sT1";
let readalongId;

describe("manage readalongs", () => {
  before((done) => {
    instance
      .create("User", {
        naam: "test",
        gebruikersnaam: "test123",
        wachtwoord: "t@sT1",
        email: "test@test.com",
        role: "testUser",
      })
      .then((user) => {
        userId = user.get("user_id");
        done();
      });
  });

  after((done) => {
    instance.first("User", { naam: "test" }).then((user) => {
      user.delete();
      done();
    });
  });

  describe("POST /api/readalongs", () => {
    it("Should return a valid error when value is missing", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .post("/api/readalongs")
            .set("authorization", "Bearer " + token)
            .send({
              startDatum: "2021-12-1",
              eindDatum: "2021-12-17",
              readalongType: "open",
              minimunDeelnemers: 2,
              boekId: "61add84d98bce652d15641d7",
              id: userId,
            })
            .end((err, res) => {
              res.should.have.status(500);
              res.should.be.an("object");

              let message = res.text;
              message.should.be
                .a("string")
                .that.equals("create readalong failed");

              done();
            });
        }
      );
    });

    it("should return a valid response when created successfully", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .post("/api/readalongs")
            .set("authorization", "Bearer " + token)
            .send({
              naam: "Name of the Wind readalong",
              startDatum: "2021-12-1",
              eindDatum: "2021-12-17",
              readalongType: "open",
              minimunDeelnemers: 2,
              boekId: "61add84d98bce652d15641d7",
              id: userId,
            })
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");

              let obj = res.body;
              obj.naam.should.be
                .a("string")
                .that.equals("Name of the Wind readalong");
              obj.startDatum.should.be
                .a("string")
                .that.equals("2021-11-30T23:00:00.000Z");
              obj.eindDatum.should.be
                .a("string")
                .that.equals("2021-12-17T00:00:00.000Z");
              obj.readalongType.should.be.a("string").that.equals("open");
              obj.minimunDeelnemers.should.be.a("number").that.equals(2);
              obj.boekId.should.be
                .a("string")
                .that.equals("61add84d98bce652d15641d7");

              done();
            });
        }
      );
    });
  });

  describe("GET /api/readalongs", () => {
    before((done) => {
      const testReadalong = new Readalong({
        naam: "Name of the Wind readalong",
        startDatum: "2021-12-1",
        eindDatum: "2021-12-17",
        readalongType: "open",
        minimunDeelnemers: 2,
        boekId: "61add84d98bce652d15641d7",
      });

      testReadalong.save().then(() => {
        readalongId = testReadalong._id.toString();
        instance
          .create("Readalong", {
            readalong_mongo_id: readalongId,
          })
          .then((neoReadalong) => {
            instance.first("User", { user_id: userId }).then((user) => {
              neoReadalong.relateTo(user, "created");
              user.relateTo(neoReadalong, "creator");
              logger.info(neoReadalong + " is made by " + user);
            });
          });

        done();
      });
    });

    after((done) => {
      Readalong.findByIdAndDelete(readalongId)
        .lean()
        .exec(function (err, docs) {
          logger.info("readalong deleted");
          instance
            .first("Readalong", { readalong_mongo_id: readalongId })
            .then((neoReadalong) => {
              instance.first("User", { user_id: userId }).then((user) => {
                neoReadalong.detachFrom(user);
                user.detachFrom(neoReadalong);
                neoReadalong.delete();
              });
            });
          done();
        });
    });

    it("should return a valid response with 1 readalong", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .get("/api/readalongs")
            .set("authorization", "Bearer " + token)
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");

              let obj = res.body[0];
              obj.naam.should.be
                .a("string")
                .that.equals("Name of the Wind readalong");
              obj.startDatum.should.be
                .a("string")
                .that.equals("2021-11-30T23:00:00.000Z");
              obj.eindDatum.should.be
                .a("string")
                .that.equals("2021-12-17T00:00:00.000Z");
              obj.readalongType.should.be.a("string").that.equals("open");
              obj.minimunDeelnemers.should.be.a("number").that.equals(2);
              obj.boekId.should.be
                .a("string")
                .that.equals("61add84d98bce652d15641d7");

              done();
            });
        }
      );
    });
  });

  describe("GET /api/readalongs/:id", () => {
    before((done) => {
      const testReadalong = new Readalong({
        naam: "Name of the Wind readalong",
        startDatum: "2021-12-1",
        eindDatum: "2021-12-17",
        readalongType: "open",
        minimunDeelnemers: 2,
        boekId: "61add84d98bce652d15641d7",
      });

      testReadalong.save().then(() => {
        readalongId = testReadalong._id.toString();
        instance
          .create("Readalong", {
            readalong_mongo_id: readalongId,
          })
          .then((neoReadalong) => {
            instance.first("User", { user_id: userId }).then((user) => {
              neoReadalong.relateTo(user, "created");
              user.relateTo(neoReadalong, "creator");
              logger.info(neoReadalong + " is made by " + user);
            });
          });

        done();
      });
    });

    after((done) => {
      Readalong.findByIdAndDelete(readalongId)
        .lean()
        .exec(function (err, docs) {
          logger.info("readalong deleted");
          instance
            .first("Readalong", { readalong_mongo_id: readalongId })
            .then((neoReadalong) => {
              instance.first("User", { user_id: userId }).then((user) => {
                neoReadalong.detachFrom(user);
                user.detachFrom(neoReadalong);
                neoReadalong.delete();
              });
            });
          done();
        });
    });

    it("should return a valid response with empty body when readalong doesn't exist", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .get("/api/readalongs/0")
            .set("authorization", "Bearer " + token)
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");

              logger.info(res.body);
              let obj = res.body;
              obj.should.be.an("object");

              done();
            });
        }
      );
    });

    it("should return a valid response when readlong exists", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .get("/api/readalongs/" + readalongId)
            .set("authorization", "Bearer " + token)
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");

              let obj = res.body;
              obj.naam.should.be
                .a("string")
                .that.equals("Name of the Wind readalong");
              obj.startDatum.should.be
                .a("string")
                .that.equals("2021-11-30T23:00:00.000Z");
              obj.eindDatum.should.be
                .a("string")
                .that.equals("2021-12-17T00:00:00.000Z");
              obj.readalongType.should.be.a("string").that.equals("open");
              obj.minimunDeelnemers.should.be.a("number").that.equals(2);
              obj.boekId.should.be
                .a("string")
                .that.equals("61add84d98bce652d15641d7");

              done();
            });
        }
      );
    });
  });

  describe("PUT /api/readalongs/:id", () => {
    before((done) => {
      const testReadalong = new Readalong({
        naam: "Name of the Wind readalong",
        startDatum: "2021-12-1",
        eindDatum: "2021-12-17",
        readalongType: "open",
        minimunDeelnemers: 2,
        boekId: "61add84d98bce652d15641d7",
      });

      testReadalong.save().then(() => {
        readalongId = testReadalong._id.toString();
        instance
          .create("Readalong", {
            readalong_mongo_id: readalongId,
          })
          .then((neoReadalong) => {
            instance.first("User", { user_id: userId }).then((user) => {
              neoReadalong.relateTo(user, "created");
              user.relateTo(neoReadalong, "creator");
              logger.info(neoReadalong + " is made by " + user);
            });
          });

        done();
      });
    });

    after((done) => {
      Readalong.findByIdAndDelete(readalongId)
        .lean()
        .exec(function (err, docs) {
          logger.info("readalong deleted");
          instance
            .first("Readalong", { readalong_mongo_id: readalongId })
            .then((neoReadalong) => {
              instance.first("User", { user_id: userId }).then((user) => {
                neoReadalong.detachFrom(user);
                user.detachFrom(neoReadalong);
                neoReadalong.delete();
              });
            });
          done();
        });
    });

    it("should return a valid response when no body is found", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .put("/api/readalongs/" + readalongId)
            .set("authorization", "Bearer " + token)
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");

              let obj = res.body;
              obj.naam.should.be
                .a("string")
                .that.equals("Name of the Wind readalong");
              obj.startDatum.should.be
                .a("string")
                .that.equals("2021-11-30T23:00:00.000Z");
              obj.eindDatum.should.be
                .a("string")
                .that.equals("2021-12-17T00:00:00.000Z");
              obj.readalongType.should.be.a("string").that.equals("open");
              obj.minimunDeelnemers.should.be.a("number").that.equals(2);
              obj.boekId.should.be
                .a("string")
                .that.equals("61add84d98bce652d15641d7");

              done();
            });
        }
      );
    });

    it("should return a valid response when body is found", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .get("/api/readalongs/" + readalongId)
            .set("authorization", "Bearer " + token)
            .send({
              naam: "doors of stone readalong",
            })
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");

              let obj = res.body;
              obj.naam.should.be
                .a("string")
                .that.equals("Name of the Wind readalong");
              obj.startDatum.should.be
                .a("string")
                .that.equals("2021-11-30T23:00:00.000Z");
              obj.eindDatum.should.be
                .a("string")
                .that.equals("2021-12-17T00:00:00.000Z");
              obj.readalongType.should.be.a("string").that.equals("open");
              obj.minimunDeelnemers.should.be.a("number").that.equals(2);
              obj.boekId.should.be
                .a("string")
                .that.equals("61add84d98bce652d15641d7");

              done();
            });
        }
      );
    });
  });

  describe("DELETE /api/readalongs/:id", () => {
    before((done) => {
      const testReadalong = new Readalong({
        naam: "Name of the Wind readalong",
        startDatum: "2021-12-1",
        eindDatum: "2021-12-17",
        readalongType: "open",
        minimunDeelnemers: 2,
        boekId: "61add84d98bce652d15641d7",
      });

      testReadalong.save().then(() => {
        readalongId = testReadalong._id.toString();
        instance
          .create("Readalong", {
            readalong_mongo_id: readalongId,
          })
          .then((neoReadalong) => {
            instance.first("User", { user_id: userId }).then((user) => {
              neoReadalong.relateTo(user, "created");
              user.relateTo(neoReadalong, "creator");
              logger.info(neoReadalong + " is made by " + user);
            });
          });

        done();
      });
    });

    after((done) => {
      Readalong.findByIdAndDelete(readalongId)
        .lean()
        .exec(function (err, docs) {
          logger.info("readalong deleted");
          instance
            .first("Readalong", { readalong_mongo_id: readalongId })
            .then((neoReadalong) => {
              instance.first("User", { user_id: userId }).then((user) => {
                neoReadalong.detachFrom(user);
                user.detachFrom(neoReadalong);
                neoReadalong.delete();
              });
            });
          done();
        });
    });

    it("should return a vaild error when readalong doens\'t exist", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .put("/api/readalongs/0")
            .set("authorization", "Bearer " + token)
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");

              done();
            });
        }
      );
    });

    xit("should return a valid response when readalong exits", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .delete("/api/readalongs/" + readalongId)
            .set("authorization", "Bearer " + token)
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");

              done();
            });
        }
      );
    });
  });

  describe("POST /api/readalongs/:id/members/register", () => {
    before((done) => {
      const testReadalong = new Readalong({
        naam: "Name of the Wind readalong",
        startDatum: "2021-12-1",
        eindDatum: "2021-12-17",
        readalongType: "open",
        minimunDeelnemers: 2,
        boekId: "61add84d98bce652d15641d7",
      });

      testReadalong.save().then(() => {
        readalongId = testReadalong._id.toString();
        instance
          .create("Readalong", {
            readalong_mongo_id: readalongId,
          })
          .then((neoReadalong) => {
            instance.first("User", { user_id: userId }).then((user) => {
              neoReadalong.relateTo(user, "created");
              user.relateTo(neoReadalong, "creator");
              logger.info(neoReadalong + " is made by " + user);
            });
          });

        done();
      });
    });

    after((done) => {
      Readalong.findByIdAndDelete(readalongId)
        .lean()
        .exec(function (err, docs) {
          logger.info("readalong deleted");
          instance
            .first("Readalong", { readalong_mongo_id: readalongId })
            .then((neoReadalong) => {
              instance.first("User", { user_id: userId }).then((user) => {
                neoReadalong.detachFrom(user);
                user.detachFrom(neoReadalong);
                neoReadalong.delete();
              });
            });
          done();
        });
    });

    xit("should return a valid error when no body is available", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .put("/api/readalongs/" + readalongId + "/members/register")
            .set("authorization", "Bearer " + token)
            .end((err, res) => {
              res.should.have.status(500);
              res.should.be.an("object");

              let message = res.text;
              message.should.equal("create readalong failed");

              done();
            });
        }
      );
    });

    xit("should return a valid response when body is available", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .put("/api/readalongs/" + readalongId + "/members/register")
            .set("authorization", "Bearer " + token)
            .send({
              id: userId,
            })
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");

              done();
            });
        }
      );
    });
  });

  describe("GET /api/readalongs/:id/members", () => {
    before((done) => {
      const testReadalong = new Readalong({
        naam: "Name of the Wind readalong",
        startDatum: "2021-12-1",
        eindDatum: "2021-12-17",
        readalongType: "open",
        minimunDeelnemers: 2,
        boekId: "61add84d98bce652d15641d7",
      });

      testReadalong.save().then(() => {
        readalongId = testReadalong._id.toString();
        instance
          .create("Readalong", {
            readalong_mongo_id: readalongId,
          })
          .then((neoReadalong) => {
            instance.first("User", { user_id: userId }).then((user) => {
              neoReadalong.relateTo(user, "created");
              user.relateTo(neoReadalong, "creator");
              user.relateTo(neoReadalong, "enter");
              neoReadalong.relateTo(user, "joined");
              logger.info(neoReadalong + " is made by " + user);
            });
          });

        done();
      });
    });

    after((done) => {
      Readalong.findByIdAndDelete(readalongId)
        .lean()
        .exec(function (err, docs) {
          logger.info("readalong deleted");
          instance
            .first("Readalong", { readalong_mongo_id: readalongId })
            .then((neoReadalong) => {
              instance.first("User", { user_id: userId }).then((user) => {
                neoReadalong.detachFrom(user);
                user.detachFrom(neoReadalong);
                user.detachFrom(neoReadalong);
                neoReadalong.detachFrom(user);
                neoReadalong.delete();
              });
            });
          done();
        });
    });

    xit("should return a valid error when user doesn't exist", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .put("/api/readalongs/0/members")
            .set("authorization", "Bearer " + token)
            .end((err, res) => {
              res.should.have.status(500);
              res.should.be.an("object");

              let message = res.text;
              message.should.equal("create readalong failed");

              done();
            });
        }
      );
    });

    xit("should return a valid result when user exists", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .put("/api/readalongs/" + readalongId + "/members")
            .set("authorization", "Bearer " + token)
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");

              done();
            });
        }
      );
    });
  });

  describe("PUT /api/readalongs/:id/members/unregister", () => {
    before((done) => {
      const testReadalong = new Readalong({
        naam: "Name of the Wind readalong",
        startDatum: "2021-12-1",
        eindDatum: "2021-12-17",
        readalongType: "open",
        minimunDeelnemers: 2,
        boekId: "61add84d98bce652d15641d7",
      });

      testReadalong.save().then(() => {
        readalongId = testReadalong._id.toString();
        instance
          .create("Readalong", {
            readalong_mongo_id: readalongId,
          })
          .then((neoReadalong) => {
            instance.first("User", { user_id: userId }).then((user) => {
              neoReadalong.relateTo(user, "created");
              user.relateTo(neoReadalong, "creator");
              user.relateTo(neoReadalong, "enter");
              neoReadalong.relateTo(user, "joined");
              logger.info(neoReadalong + " is made by " + user);
            });
          });

        done();
      });
    });

    after((done) => {
      Readalong.findByIdAndDelete(readalongId)
        .lean()
        .exec(function (err, docs) {
          logger.info("readalong deleted");
          instance
            .first("Readalong", { readalong_mongo_id: readalongId })
            .then((neoReadalong) => {
              instance.first("User", { user_id: userId }).then((user) => {
                neoReadalong.detachFrom(user);
                user.detachFrom(neoReadalong);
                user.detachFrom(neoReadalong);
                neoReadalong.detachFrom(user);
                neoReadalong.delete();
              });
            });
          done();
        });
    });

    it("should return a valid error when no user exits", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .put("/api/readalongs/" + readalongId + "/members/unregister")
            .set("authorization", "Bearer " + token)
            .end((err, res) => {
              res.should.have.status(500);
              res.should.be.an("object");

              let message = res.text;
              message.should.equal("create readalong failed");

              done();
            });
        }
      );
    });

    it("Should return a valid response when user succesfully deleted", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .put("/api/readalongs/" + readalongId + "/members/unregister")
            .set("authorization", "Bearer " + token)
            .send({
              id: userId,
            })
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");

              done();
            });
        }
      );
    });
  });
});
