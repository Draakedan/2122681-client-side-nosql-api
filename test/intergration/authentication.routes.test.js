const config = require("../../src/config/config");
const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../server");
const tracer = require("tracer");
const instance = require("../../src/config/databaseConnect");

const logger = config.logger;
chai.should();
chai.use(chaiHttp);

describe("manage authentication", () => {
  before((done) => {
    instance
      .create("User", {
        naam: "test",
        gebruikersnaam: "test123",
        wachtwoord: "t@sT1",
        email: "test@test.com",
        role: "testUser",
      })
      .then((user) => {
        done();
      });
  });

  after((done) => {
    instance.first("User", { naam: "test" }).then((user) => {
      user.delete();
      done();
    });
  });

  describe("POST - /api/login", () => {
    it("should return a valid error when required field is missing", (done) => {
      chai
        .request(server)
        .post("/api/login")
        .send({
          ww: "t@sT1",
        })
        .end((err, res) => {
          res.should.have.status(500);
          res.should.be.an("object");

          let message = res.text;
          message.should.be.a("string").that.equals("Login failed");

          done();
        });
    });

    it("should return a valid response when succesfully logged in", (done) => {
      chai
        .request(server)
        .post("/api/login")
        .send({
          gn: "test123",
          ww: "t@sT1",
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.should.be.an("object");

          const response = res.body;

          response.should.have.property("token").which.is.a("string");
          response.should.have.property("gebruikersnaam").which.is.a("string");
          done();
        });
    });
  });
});
