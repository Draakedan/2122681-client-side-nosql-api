const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../server");
const tracer = require("tracer");
const config = require("../../src/config/config");
const jwt = require("jsonwebtoken");
const instance = require("../../src/config/databaseConnect");
const Book = require("../../src/config/bookDatabase");

const logger = config.logger;

chai.should();
chai.use(chaiHttp);
tracer.setLevel("error");

let userId;
let userName = "test123";
let userPass = "t@sT1";
let bookId;

describe("manage books", () => {
  before((done) => {
    instance
      .create("User", {
        naam: "test",
        gebruikersnaam: "test123",
        wachtwoord: "t@sT1",
        email: "test@test.com",
        role: "testUser",
      })
      .then((user) => {
        userId = user.get("user_id");
        done();
      });
  });

  after((done) => {
    instance.first("User", { naam: "test" }).then((user) => {
      user.delete();
      done();
    });
  });

  describe("POST /api/books", () => {
    it("should return a valid error when value is missing", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .post("/api/books")
            .set("authorization", "Bearer " + token)
            .send({
              autheur: "patrick Rothfuss",
              isbn: 2,
              eersteUitgave: "2008-08-28",
              paginas: 394,
              genre: "Epic fantasy",
              id: userId,
            })
            .end((err, res) => {
              res.should.have.status(500);
              res.should.be.an("object");

              let message = res.text;
              message.should.be.a("string").that.equals("create book failed");

              done();
            });
        }
      );
    });

    it("should return a valid response when book is added", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .post("/api/books")
            .set("authorization", "Bearer " + token)
            .send({
              titel: "Name of the Wind",
              autheur: "patrick Rothfuss",
              isbn: 2,
              eersteUitgave: "2008-08-28",
              paginas: 394,
              genre: "Epic fantasy",
              id: userId,
            })
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");

              let obj = res.body;
              obj.titel.should.be.a("string").that.equals("Name of the Wind");
              obj.autheur.should.be.a("string").that.equals("patrick Rothfuss");
              obj.isbn.should.be.a("number").that.equals(2);
              obj.eersteUitgave.should.be
                .a("string")
                .that.equals("2008-08-28T00:00:00.000Z");
              obj.paginas.should.be.a("number").that.equals(394);
              obj.genre.should.be.a("string").that.equals("Epic fantasy");

              done();
            });
        }
      );
    });
  });

  describe("GET /api/books", () => {
    before((done) => {
      const testBook = new Book({
        titel: "Name of the Wind",
        autheur: "patrick Rothfuss",
        isbn: 2,
        eersteUitgave: "2008-08-28",
        paginas: 394,
        genre: "Epic fantasy",
      });

      testBook.save().then(() => {
        bookId = testBook._id.toString();
        instance.create("Book", { book_mongo_id: bookId }).then((neoBook) => {
          instance.first("User", { user_id: userId }).then((user) => {
            neoBook.relateTo(user, "created");
            user.relateTo(neoBook, "creator");
            logger.info(neoBook + " is made by " + user);
          });
        });

        done();
      });
    });

    after((done) => {
      Book.findByIdAndDelete(bookId)
        .lean()
        .exec(function (err, docs) {
          instance.first("Book", { book_mongo_id: bookId }).then((neoBook) => {
            instance.first("User", { user_id: userId }).then((user) => {
              neoBook.detachFrom(user);
              user.detachFrom(neoBook);
              neoBook.delete();
            });
          });

          done();
        });
    });

    it("should return a valid response with 1 book", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .get("/api/books")
            .set("authorization", "Bearer " + token)
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");

              let obj = res.body[0];

              obj.titel.should.be.a("string");
              obj.autheur.should.be.a("string");
              obj.isbn.should.be.a("number");
              obj.eersteUitgave.should.be.a("string");
              obj.paginas.should.be.a("number");
              obj.genre.should.be.a("string");

              done();
            });
        }
      );
    });
  });

  describe("GET /api/books/:id", () => {
    before((done) => {
      const testBook = new Book({
        titel: "Name of the Wind",
        autheur: "patrick Rothfuss",
        isbn: 2,
        eersteUitgave: "2008-08-28",
        paginas: 394,
        genre: "Epic fantasy",
      });

      testBook.save().then(() => {
        bookId = testBook._id.toString();
        instance.create("Book", { book_mongo_id: bookId }).then((neoBook) => {
          instance.first("User", { user_id: userId }).then((user) => {
            neoBook.relateTo(user, "created");
            user.relateTo(neoBook, "creator");
          });
        });

        done();
      });
    });

    after((done) => {
      Book.findByIdAndDelete(bookId)
        .lean()
        .exec(function (err, docs) {
          instance.first("Book", { book_mongo_id: bookId }).then((neoBook) => {
            instance.first("User", { user_id: userId }).then((user) => {
              neoBook.detachFrom(user);
              user.detachFrom(neoBook);
              neoBook.delete();
            });
          });

          done();
        });
    });

    it("should return an empty object when requested book doesn't exist", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .get("/api/books/0")
            .set("authorization", "Bearer " + token)
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");

              done();
            });
        }
      );
    });

    it("should return a vaild response when requresed book exists", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .get("/api/books/" + bookId)
            .set("authorization", "Bearer " + token)
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");

              let obj = res.body;

              obj.titel.should.be.a("string").that.equals("Name of the Wind");
              obj.autheur.should.be.a("string").that.equals("patrick Rothfuss");
              obj.isbn.should.be.a("number").that.equals(2);
              obj.eersteUitgave.should.be
                .a("string")
                .that.equals("2008-08-28T00:00:00.000Z");
              obj.paginas.should.be.a("number").that.equals(394);
              obj.genre.should.be.a("string").that.equals("Epic fantasy");

              done();
            });
        }
      );
    });
  });

  describe("PUT /api/books/:id", () => {
    before((done) => {
      const testBook = new Book({
        titel: "Name of the Wind",
        autheur: "patrick Rothfuss",
        isbn: 2,
        eersteUitgave: "2008-08-28",
        paginas: 394,
        genre: "Epic fantasy",
      });

      testBook.save().then(() => {
        bookId = testBook._id.toString();
        instance.create("Book", { book_mongo_id: bookId }).then((neoBook) => {
          instance.first("User", { user_id: userId }).then((user) => {
            neoBook.relateTo(user, "created");
            user.relateTo(neoBook, "creator");
          });
        });

        done();
      });
    });

    after((done) => {
      Book.findByIdAndDelete(bookId)
        .lean()
        .exec(function (err, docs) {
          instance.first("Book", { book_mongo_id: bookId }).then((neoBook) => {
            instance.first("User", { user_id: userId }).then((user) => {
              neoBook.detachFrom(user);
              user.detachFrom(neoBook);
              neoBook.delete();
            });
          });

          done();
        });
    });

    it("should return a valid error when body is empty", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .put("/api/books/" + bookId)
            .set("authorization", "Bearer " + token)
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");

              let obj = res.body;
              obj.autheur.should.be.a("string").that.equals("patrick Rothfuss");

              done();
            });
        }
      );
    });

    it("should return a valid response when succesfully updated", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .put("/api/books/" + bookId)
            .set("authorization", "Bearer " + token)
            .send({
              autheur: "Brandon Sanderson",
              id: userId,
            })
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");

              let obj = res.body;
              obj.autheur.should.be.a("string").that.equals("patrick Rothfuss");

              done();
            });
        }
      );
    });
  });

  describe("DELETE /api/books/:id", () => {
    before((done) => {
      const testBook = new Book({
        titel: "Name of the Wind",
        autheur: "patrick Rothfuss",
        isbn: 2,
        eersteUitgave: "2008-08-28",
        paginas: 394,
        genre: "Epic fantasy",
      });

      testBook.save().then(() => {
        bookId = testBook._id.toString();
        instance.create("Book", { book_mongo_id: bookId }).then((neoBook) => {
          instance.first("User", { user_id: userId }).then((user) => {
            neoBook.relateTo(user, "created");
            user.relateTo(neoBook, "creator");
          });
        });

        done();
      });
    });

    it("should return a valid error when book doens't exist", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .delete("/api/books/0")
            .set("authorization", "Bearer " + token)
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");
              done();
            });
        }
      );
    });

    it("should return a valid response when book is deleted", (done) => {
      jwt.sign(
        {
          gebruikersnaam: userName,
          wacthwoord: userPass,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1h" },
        (err, token) => {
          chai
            .request(server)
            .delete("/api/books/" + bookId)
            .set("authorization", "Bearer " + token)
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.an("object");

              done();
            });
        }
      );
    });
  });
});
